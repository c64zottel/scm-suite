use std::fs::File;
use std::io::BufReader;
use clap::Parser;
use serde_json::Value;
use scm_lib::scjson_serde::RootElement;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(short, long)]
    scjson: String,
}



fn main() {
    let args = Args::parse();
    let json = read_json(&args.scjson);

    let p = serde_json::from_value::<RootElement>(json.to_string().parse().unwrap()).unwrap();

    for s in &p.scjson {
        println!("{}", s.state_machine);
        for sm in &s.states {
            println!("{:?}", sm);
        }
    }
    println!("hello {:?}", json);
    println!("hello {:?}", p);
}