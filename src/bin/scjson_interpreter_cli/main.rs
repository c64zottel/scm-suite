use std::fs::File;
use std::io::BufReader;
use clap::Parser;
use scm_lib::init_state_machines_from_json;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(short = 'j', long)]
    scjson: String,
    #[arg(short = 'l', long)]
    host: Option<String>,
    #[arg(short = 's', long)]
    start_server: Option<bool>,
}

fn read_json(path: &String) -> serde_json::Value {
    let file = File::open(&path).expect("Failed to open file");
    let reader = BufReader::new(file);
    serde_yaml::from_reader(reader).unwrap()
}

//TODO: Prepare for Keyboard-Plugin:
// - switch to disable
// - define json format for Plugins
// - file structure?
// - load on demand? (Plugins may not be de-loaded (?))
//TODO maybe make the lib tokio only: https://tokio.rs/tokio/topics/bridging
#[tokio::main]
async fn main() {
    let args = Args::parse();
    let json = read_json(&args.scjson);

    let (runtime, _rec) = init_state_machines_from_json(json);

    println!("------------------------------------------------");
    println!("  go! {}", runtime);

    runtime.send_event("SM1", "E");

    runtime.block_until_finished();


    // p.scjson.iter().for_each(|sm| println!("{:?}", sm.state_machine));
    // let bind_address = &args.host.unwrap_or("127.0.0.1:42000".to_string());
    // let listener = TcpListener::bind(bind_address).await.unwrap();
    // loop {
    //     // The second item contains the IP and port of the new connection.
    //     let (socket, ip_port) = listener.accept().await.unwrap();
    //     println!("new connection: {:?}", ip_port);
    //     process(socket).await;
    // }

    // println!("hello {:?} {:?}", ip_address, p)
}