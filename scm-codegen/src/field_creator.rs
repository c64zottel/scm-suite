use std::str::FromStr;
use proc_macro2::TokenStream;
use quote::quote;
use crate::{resolve_def_reference, Schema, SimpleTypes, TypeDefinitions};
use heck::{ToSnakeCase, ToUpperCamelCase};

pub type TypeSchema = (String, Schema);
pub type TypeSchemas = Vec<TypeSchema>;

type FieldType = String;
type FieldName = String;
type Optional = bool;

pub(crate) enum FieldDefinition {
    Normal(Optional, FieldType, FieldName),
    Vector(FieldType, FieldName),
}

pub fn compose_tokenstream(identifier: &str, field_type: &str, original_identifier: &str) -> (TokenStream, TokenStream, Option<TokenStream>) {
    let ft = TokenStream::from_str(&field_type).unwrap();

    let annotation = (identifier != original_identifier)
        .then(|| quote! { #[serde(rename = #original_identifier)] });

    let ident = TokenStream::from_str(identifier).unwrap();

    (ident, ft, annotation)
}

impl FieldDefinition {
    pub(crate) fn create_tokenstream(&self) -> TokenStream {
        let mut field_annotation = None;

        let (identifier, field_type, annotation) =
            match self {
                FieldDefinition::Normal(optional, ft, field_name) => {
                    println!("NORMAL {field_name} {optional}");
                    let original_identifier = field_name.to_owned();
                    let identifier = field_name.to_snake_case();
                    let mut field_type = ft.to_owned();
                    if *optional {
                        field_type = format!("Option<{}>", field_type);
                    } else { // This is for ExecutableContent - how to make it prettier?
                        field_annotation = Some(TokenStream::from_str("#[serde(default)]").unwrap());
                    }
                    compose_tokenstream(&identifier, &field_type, &original_identifier)
                }
                FieldDefinition::Vector(ft, field_name) => {
                    let original_identifier = field_name.to_owned();
                    let identifier = field_name.to_snake_case();
                    let field_type = format!("Vec<{}>", ft.to_upper_camel_case());
                    field_annotation = Some(TokenStream::from_str("#[serde(default)]").unwrap());
                    compose_tokenstream(&identifier, &field_type, &original_identifier)
                }
            };

        return quote! {
            #annotation
            #field_annotation
            pub #identifier : #field_type
        };
    }
}

pub(crate) struct FieldCreator<'a> {
    type_definitions: &'a TypeDefinitions,
    pub required_fields: Vec<String>,
    pub potential_new_types: TypeSchemas, // make iterator
}

pub enum ItemType {
    MultiplierOneOf(Vec<Schema>),
    Type(TypeSchema),
}

pub fn collect_types_of_schema(schema: &Schema) -> TypeSchemas {
    if schema.type_[0].eq(&SimpleTypes::Object) {
        return schema.properties.to_owned().into_iter().collect::<TypeSchemas>();
    }
    panic!("not implemented, only type_[0]")
}

// Defining the type of an item is a non-trivial task.
// if items have exactly one type, we can have a Vector with that type
pub fn find_types_of_items(type_definitions: &TypeDefinitions, schema: &Schema) -> ItemType {
    let schema = schema.items.clone().expect("no schema in items given.");

    if let Some(reference) = &schema.ref_ {
        let (type_name, type_definition) = resolve_def_reference(reference, type_definitions);
        if !type_definition.contains_key(type_name) { panic!("\nType ({}) not found in {:?}", type_name, type_definition) }
        return ItemType::Type((type_name.to_owned(), type_definition[type_name].to_owned()));
    }

    if !schema.one_of.is_empty() {
        return ItemType::MultiplierOneOf(schema.one_of.to_owned());
    }
    panic!("only ref and one_of allowed {:?}", schema);
}


impl<'a> FieldCreator<'a> {
    pub(crate) fn new(type_definitions: &'a TypeDefinitions, schema: &Schema) -> FieldCreator<'a> {
        let required_fields = schema.required.to_owned();
        Self {
            type_definitions,
            required_fields,
            potential_new_types: vec![],
        }
    }

    pub(crate) fn determine_field_type(&mut self, schema: &Schema, field_name: &String) -> FieldDefinition {
        println!("determining field_type: {field_name} {:?}", schema);

        let mut is_optional = !self.required_fields.contains(&field_name);
        if let Some(reference) = &schema.ref_ {
            let (type_name, type_definition) = resolve_def_reference(&reference, self.type_definitions);
            self.potential_new_types.push((type_name.to_string(), type_definition[type_name].to_owned()));
            if is_optional { //only try overwrite if it is not explicitly required
                if type_definition[type_name].to_owned().type_[0].eq(&SimpleTypes::Array)
                { is_optional = false; } //overwrite 'false' because we are referring to an array
            }
            return FieldDefinition::Normal(is_optional, type_name.to_string(), field_name.to_string());
        }

        // more then 1 type should become a struct
        if schema.type_.len() != 1 { panic!("multi schema.type_ not supported yet {:?}", schema); }
        match schema.type_[0] {
            SimpleTypes::Array => {
                println!(">ITEMS: {field_name} {:?}", &schema);
                return match find_types_of_items(self.type_definitions, &schema) {
                    ItemType::Type(type_schema) => {
                        println!("{:?}", type_schema);
                        self.potential_new_types.push(type_schema.to_owned());
                        FieldDefinition::Vector(type_schema.0.to_string(), field_name.to_owned())
                    }
                    _ => { panic!("not impl") }
                };
            }
            SimpleTypes::Boolean => {}
            SimpleTypes::Number => {}
            SimpleTypes::Null => {}
            SimpleTypes::Integer => {
                return FieldDefinition::Normal(is_optional, String::from("i64"), field_name.to_owned());
            }
            SimpleTypes::String => {
                return FieldDefinition::Normal(is_optional, String::from("String"), field_name.to_owned());
            }
            SimpleTypes::Object => {
                if schema.properties.is_empty() { panic!("type object without properties?"); }
                self.potential_new_types.push((field_name.to_owned(), schema.to_owned()));
                return FieldDefinition::Normal(is_optional, field_name.to_upper_camel_case().to_owned(), field_name.to_owned());
            }
        }

        panic!("Cannot determine type {:?}", schema);
    }
}
