use proc_macro2::TokenStream;
use quote::TokenStreamExt;
use crate::runtime::Environment;
use crate::schema_object::Schema;
use crate::{create_objects, TypeDictionary};

#[test]
fn t3() {
    let kword = "RootElement";
    let json = r###"
{
  "$id": "urn:scjson-schema-0.1",
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "$defs": {
    "IdType": { "type": "string" },
    "StateTypes": {
      "oneOf": [
        {
          "type": "object",
          "additionalProperties": false,
          "properties": {
            "State": {
              "type": "object",
              "required": ["id"],
              "additionalProperties": false,
              "properties": {
                "id": { "$ref": "#/$defs/IdType" }
              }
            }
          }
        }
      ]
    },
    "StateMachineType": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "StateMachine": { "type": "string" },
        "states": {
          "type": "array",
          "items": { "$ref": "#/$defs/StateTypes" },
          "minItems": 1
        }
      },
      "required": ["StateMachine", "states"]
    }
  },
  "title": "root",
  "type": "array",
  "description": "Statechart's root element.",
  "items": { "$ref": "#/$defs/StateMachineType" },
  "minItems": 1
}
"###;

    let schema: Schema = serde_json::from_str(json).unwrap();
    let mut environment = Environment {
        type_defs: schema.definitions.to_owned(),
        type_dict: TypeDictionary::new(),
    };
    create_objects(&kword.to_string(), &schema, &mut environment);

    println!("---\n");

    let mut ts = TokenStream::new();
    ts.append_all(environment.type_dict.values());
    let expected_result = r###"pub type IdType = String;
pub type RootElement = Vec<StateMachineType>;
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize, Default)]
#[serde(deny_unknown_fields)]
pub struct State {
    #[serde(default)]
    pub id: IdType,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize, Default)]
#[serde(deny_unknown_fields)]
pub struct StateMachineType {
    #[serde(rename = "StateMachine")]
    #[serde(default)]
    pub state_machine: String,
    #[serde(default)]
    pub states: Vec<StateTypes>,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub enum StateTypes {
    State(State),
}
"###;
    let ts_file = syn::parse_file(&ts.to_string()).unwrap();

    let actual_output = prettyplease::unparse(&ts_file);
    println!("{}", actual_output);

    assert_eq!(actual_output, expected_result.to_string());
}

#[test]
fn t2() {
    let kword = "RootElement";
    let json = r###"
{
  "$id": "urn:scjson-schema-0.1",
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "$defs": {
    "StateTypes": { "type": "string" },
    "StateMachineType": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "StateMachine": { "type": "string" },
        "states": {
          "type": "array",
          "items": { "$ref": "#/$defs/StateTypes" },
          "minItems": 1
        }
      },
      "required": ["StateMachine", "states"]
    }
  },
  "title": "root",
  "type": "array",
  "description": "Statechart's root element.",
  "items": { "$ref": "#/$defs/StateMachineType" },
  "minItems": 1
}
"###;

    let schema: Schema = serde_json::from_str(json).unwrap();
    let mut environment = Environment {
        type_defs: schema.definitions.to_owned(),
        type_dict: TypeDictionary::new(),
    };

    create_objects(&kword.to_string(), &schema, &mut environment);

    println!("---\n");

    let mut ts = TokenStream::new();
    ts.append_all(environment.type_dict.values());
    let expected_result = r###"pub type RootElement = Vec<StateMachineType>;
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize, Default)]
#[serde(deny_unknown_fields)]
pub struct StateMachineType {
    #[serde(rename = "StateMachine")]
    #[serde(default)]
    pub state_machine: String,
    #[serde(default)]
    pub states: Vec<StateTypes>,
}
pub type StateTypes = String;
"###;
    let ts_file = syn::parse_file(&ts.to_string()).unwrap();

    let text = prettyplease::unparse(&ts_file);
    println!("{}", text);

    assert_eq!(text, expected_result.to_string());
}


#[test]
fn t1() {
    let kword = "RootElement";
    let json = r###"
{
  "$id": "urn:scjson-schema-0.1",
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "$defs": {
    "StateMachineType": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "StateMachine": { "type": "string" }
      },
      "required": ["StateMachine"]
    }
  },
  "title": "root",
  "type": "array",
  "description": "Statechart's root element.",
  "items": { "$ref": "#/$defs/StateMachineType" },
  "minItems": 1
}
"###;

    let schema: Schema = serde_json::from_str(json).unwrap();
    let mut environment = Environment {
        type_defs: schema.definitions.to_owned(),
        type_dict: TypeDictionary::new(),
    };

    create_objects(&kword.to_string(), &schema, &mut environment);

    println!("---\n");

    let mut ts = TokenStream::new();
    ts.append_all(environment.type_dict.values());
    let expected_result = r###"pub type RootElement = Vec<StateMachineType>;
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize, Default)]
#[serde(deny_unknown_fields)]
pub struct StateMachineType {
    #[serde(rename = "StateMachine")]
    #[serde(default)]
    pub state_machine: String,
}
"###;
    let ts_file = syn::parse_file(&ts.to_string()).unwrap();

    let text = prettyplease::unparse(&ts_file);
    println!("{}", text);

    assert_eq!(text, expected_result.to_string());
}