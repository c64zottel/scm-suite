use crate::schema_object::TypeDefinitions;


fn extract_path(reference: &String) -> Vec<&str> {
    //only $def is supported
    let indices = reference.split('/').skip(2).collect::<Vec<&str>>();

    if indices.len() == 0 {
        panic!("Can not handle refs without a wrapper: {}", reference);
    }

    if indices.last().eq(&Some(&"properties")) {
        panic!("A reference can not end with 'properties' {}", reference);
    }

    indices
}

// returns:
// - name of the type extracted from $ref
// - type definition BEFORE the actual type,
pub fn resolve_def_reference<'a>(reference: &'a String, type_definitions: &'a TypeDefinitions) -> (&'a str, &'a TypeDefinitions) {
    let mut indices = extract_path(reference);

    let type_name = indices.remove(indices.len() - 1);

    // We return the BTreeMap - not the Schema
    // typeDefinition[type_name] returns the type_name's schema
    let type_definition = indices.iter()
        .filter(|x| (**x).ne("properties"))
        .fold(type_definitions, |i, x| {
            &i[x.to_owned()].properties
        });

    return (type_name, type_definition);
}
