use serde::*;

// https://cswr.github.io/JsonSchema/spec/grammar/


#[derive(Clone, PartialEq, Debug, Deserialize)]
#[serde(rename = "simpleTypes")]
pub enum SimpleTypes {
    #[serde(rename = "array")]
    Array,
    #[serde(rename = "boolean")]
    Boolean,
    #[serde(rename = "integer")]
    Integer,
    #[serde(rename = "null")]
    Null,
    #[serde(rename = "number")]
    Number,
    #[serde(rename = "object")]
    Object,
    #[serde(rename = "string")]
    String,
}


use std::collections::BTreeMap;

pub type TypeDefinitions = BTreeMap<String, Schema>;


#[derive(Clone, PartialEq, Debug, Deserialize)]
pub struct Schema {
    #[serde(default)]
    pub items: Option<Box<Schema>>,
    #[serde(default)]
    #[serde(rename = "$schema")]
    pub schema: Option<String>,
    pub description: Option<String>,
    #[serde(default)]
    #[serde(rename = "$ref")]
    pub ref_: Option<String>,
    #[serde(default)]
    #[serde(rename = "$defs")]
    pub definitions: TypeDefinitions,
    #[serde(default)]
    pub properties: BTreeMap<String, Schema>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub title: Option<String>,
    #[serde(default)]
    #[serde(with = "super::type_vector")]
    #[serde(rename = "type")]
    pub type_: Vec<SimpleTypes>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "minItems")]
    pub min_items: Option<u64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "maxItems")]
    pub max_items: Option<u64>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub required: Vec<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "$id")]
    pub id: Option<String>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Vec::is_empty")]
    #[serde(rename = "allOf")]
    pub all_of: Vec<Schema>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Vec::is_empty")]
    #[serde(rename = "anyOf")]
    pub any_of: Vec<Schema>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Vec::is_empty")]
    #[serde(rename = "oneOf")]
    pub one_of: Vec<Schema>,
    //TODO: correct is: Schema
    #[serde(rename = "additionalProperties")]
    #[serde(default = "default_additional_properties")]
    pub additional_properties: bool,
    #[serde(rename = "enum")]
    #[serde(default)]
    pub enum_: Vec<serde_json::Value>,
    pub minimum: Option<i64>,
}

fn default_additional_properties() -> bool { false }
