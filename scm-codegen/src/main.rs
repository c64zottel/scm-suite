mod type_vector;
mod schema_reference;
mod schema_object;
mod field_creator;
#[cfg(test)]
mod tests;
mod runtime;

use crate::runtime::Args;
use clap::Parser;
use std::collections::BTreeMap;
use std::fs::File;
use std::io::Write;
use std::str::FromStr;
use heck::ToUpperCamelCase;
use proc_macro2::TokenStream;
use quote::{quote, TokenStreamExt};
use crate::field_creator::{compose_tokenstream, FieldCreator, collect_types_of_schema, find_types_of_items, ItemType, TypeSchemas, TypeSchema};
use crate::runtime::{create_environment, Environment};
use crate::schema_reference::resolve_def_reference;
use crate::schema_object::{Schema, SimpleTypes, TypeDefinitions};


pub type TypeDictionary = BTreeMap::<String, TokenStream>;

fn create_struct(kword: &str, schema: &Schema, environment: &mut Environment) {
    println!("Create object {kword} {:?}", schema);
    let mut fields = Vec::<TokenStream>::new();

    let mut fc = FieldCreator::new(&environment.type_defs, schema);

    for (field_name, schema) in &schema.properties {
        println!("\tprocessing field: {}", field_name);

        let field_type = fc.determine_field_type(schema, field_name);

        fields.push(field_type.create_tokenstream());
    }
    let identifier = TokenStream::from_str(kword.to_upper_camel_case().as_str()).unwrap();
    let v = quote! {
        #[derive(Clone, PartialEq, Debug, Deserialize, Serialize, Default)]
        #[serde(deny_unknown_fields)]
        pub struct #identifier { #(#fields),* }
    };

    environment.type_dict.insert(kword.to_owned(), v);

    fc.potential_new_types.iter().for_each(|(kword, schema)| { create_objects(&kword, schema, environment); });
}


fn create_enum_type(kword: &str, type_schemas: &TypeSchemas, environment: &mut Environment) {
    println!("create enum type: {kword} {:?}", type_schemas);

    let mut fields = Vec::<TokenStream>::new();
    let mut potential_new_types = Vec::<&(String, Schema)>::new();

    for type_schema in type_schemas.into_iter() {
        let kword = &type_schema.0;
        potential_new_types.push(type_schema);

        let (identifier, field_type, annotation) = compose_tokenstream(
            &kword.to_upper_camel_case(),
            &kword.to_upper_camel_case(),
            kword,
        );

        fields.push(quote! {
            #annotation
            pub #identifier(#field_type)
        });
    }

    let identifier = TokenStream::from_str(kword.to_upper_camel_case().as_str()).unwrap();
    let v = quote! {
        #[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
        #[serde(deny_unknown_fields)]
        pub enum #identifier { #(#fields),* }
    };

    environment.type_dict.insert(kword.to_owned(), v);

    potential_new_types.iter().for_each(|(kword, schema)| { create_objects(&kword, schema, environment); });
}

fn create_basic_type_alias(alias_name: &str, type_name: &str, environment: &mut Environment) {
    let ts_type_alias = TokenStream::from_str(alias_name.to_upper_camel_case().as_str()).unwrap();
    let ts_type_name = TokenStream::from_str(type_name).unwrap();

    let v = quote! {pub type #ts_type_alias = #ts_type_name;};

    environment.type_dict.insert(alias_name.to_owned(), v);
}

fn create_vec_type_alias(type_alias: &str, type_name: &str, environment: &mut Environment) {
    let ts_type_alias = TokenStream::from_str(type_alias.to_upper_camel_case().as_str()).unwrap();
    let ts_type_name = TokenStream::from_str(type_name).unwrap();

    let v = quote! {pub type #ts_type_alias = Vec<#ts_type_name>;};

    environment.type_dict.insert(type_alias.to_owned(), v);
}

//kword is defined by the schema, creates a thing out of: arrays, objects
fn create_objects(kword: &str, schema: &Schema, environment: &mut Environment) {
    if environment.type_dict.contains_key(kword) {
        println!("Type already created: {}", kword);
        return;
    }

    println!("Creating obj: {} with {:?}", kword, schema);

    match determine_target_type(schema, environment) {
        TargetType::Undefined => { panic!("Not doing so well with undefined types.") }
        TargetType::EnumType(type_schemas) => create_enum_type(kword, &type_schemas, environment),
        TargetType::StructType => create_struct(kword, schema, environment),
        TargetType::BasicType(type_name) => create_basic_type_alias(kword, &type_name, environment),
        TargetType::ArrayType(type_schema) => create_array_type(kword, type_schema, environment),
        TargetType::VecAlias(schemas) => { create_vec_alias(kword, schemas, environment) }
    };
}

fn create_array_type(type_name: &str, type_schema: TypeSchema, environment: &mut Environment) {
    let identifier = type_schema.0.to_upper_camel_case();
    create_vec_type_alias(type_name, &identifier, environment);

    create_objects(&type_schema.0.to_owned(), &type_schema.1, environment);
}


fn create_vec_alias(type_name: &str, schemas: Vec<Schema>, environment: &mut Environment) {
    let identifier = &(type_name.to_owned() + "Items");

    create_vec_type_alias(type_name, identifier, environment);

    let tt = schemas.iter().flat_map(|s| {
        collect_types_of_schema(s)
    }).collect::<TypeSchemas>();

    create_enum_type(identifier, &tt, environment);
}


// TargetType is the mapping
// from a json type to a Rust type
#[derive(Default, Clone, Debug)]
enum TargetType {
    #[default]
    Undefined,
    EnumType(TypeSchemas),
    StructType,
    BasicType(String),
    ArrayType(TypeSchema),
    VecAlias(Vec<Schema>),
}


// Mapping json types -> rust types
// string, number, integer, boolean -> String, f32, i32, bool
// null -> ?
// array -> Vec<ITEM>
// object -> Struct (Option depending on required - make it pretty)
// oneOf, allOf, anyOf -> Enum(ITEMS), Struct (no Options), Struct (all items are Optionals)
// mixed type -> Vec<Enum>?
// separate InitialTransition does not work
fn determine_target_type(schema: &Schema, environment: &mut Environment) -> TargetType {
    println!("determining type: {:?}", schema);
    let mut schema = schema;

    //When we encounter a $ref, we just eradicate it - it's not important for code generation
    if let Some(reference) = &schema.ref_ {
        let (type_name, local_type_defs) = resolve_def_reference(&reference, &environment.type_defs);
        schema = &local_type_defs[type_name];
    }

    if !schema.one_of.is_empty() {
        let ts = schema.one_of.iter().flat_map(|s| {
            collect_types_of_schema(s)
        }).collect::<TypeSchemas>();
        return TargetType::EnumType(ts);
    }


    // more then 1 type should become a struct
    if schema.type_.len() != 1 { panic!("multi schema.type_ not supported yet {:?}", schema); }
    match schema.type_[0] {
        SimpleTypes::Array => {
            println!("creating array: {:?}", schema);

            return match find_types_of_items(&environment.type_defs, &schema) {
                ItemType::MultiplierOneOf(schema) => {
                    TargetType::VecAlias(schema)
                }
                ItemType::Type(type_schema) => {
                    TargetType::ArrayType(type_schema)
                }
            };
        }
        SimpleTypes::Boolean => {}
        SimpleTypes::Integer => {}
        SimpleTypes::Null => {}
        SimpleTypes::Number => {}
        SimpleTypes::String => return TargetType::BasicType("String".to_string()),
        SimpleTypes::Object => {
            if schema.properties.is_empty() { panic!("type object without properties?"); }
            return TargetType::StructType;
        }
    }

    panic!("Cannot determine type {:?}", schema);
}

//TODO: config for SMs: prefix for log (maybe SM name?)
//TODO: global config: activate plugins (rhai)
//TODO: add type option in LOG: DEBUG, NORMAL, ???
//TODO: datamodel necessary/useful?
//TODO: in Raise: try "machine" as string or url/ip-address
//TODO: evaluate traits for enums: take_transition()
//TODO: implement "minimum" for delay, make sure
//TODO: use "default"? -> Have empty string instead of option (description in StateMachineType)
//TODO: event + target should be Vec<String>
//TODO: EC/script/language should be an enum? - how to handle plugins vs. EC vs. versions?
//TODO: split scjson.json in several files?
fn main() {
    let args = Args::parse();

    let (schema, mut environment) = create_environment(&args);

    // I don't know how to handle the start yet
    //TODO: exchange RootElement with file name? or title?
    //TODO: ExecuteEnvironment should contain the Vec<> - how to handle oneOf etc?
    create_objects(&String::from("RootElement"), &schema, &mut environment);

    let mut ts = TokenStream::new();
    ts.append_all(TokenStream::from_str("use serde_derive::*;"));
    ts.append_all(environment.type_dict.values());
    println!("-> {} ", ts.to_string());
    let ts_file = syn::parse_file(&ts.to_string()).unwrap();
    let text = prettyplease::unparse(&ts_file);
    println!("{}", text);

    args.target_file.map(|file| {
        println!("writing to target file: {:?}", file);
        File::create(file).unwrap().write_all(text.as_ref()).expect("Could not write to file.");
    });

    println!("----------------");
}

