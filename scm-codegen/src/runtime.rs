use std::fs::File;
use std::io::BufReader;
use crate::schema_object::{Schema, TypeDefinitions};
use crate::TypeDictionary;
use clap::Parser;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    #[arg(short, long)]
    pub schema: String,
    #[arg(short, long)]
    pub target_file: Option<String>,
}


pub struct Environment {
    pub type_defs: TypeDefinitions,
    pub type_dict: TypeDictionary,
}


impl Environment {
    pub fn new(json_schema: &Schema) -> Environment {
        Self {
            type_defs: json_schema.definitions.to_owned(),
            type_dict: TypeDictionary::new(),
        }
    }
}


fn read_json(path: &String) -> Schema {
    let file = File::open(&path).expect("Failed to open file");
    let reader = BufReader::new(file);
    let value = serde_json::from_reader(reader).unwrap();

    value
}

pub fn create_environment(args: &Args) -> (Schema, Environment) {
    let schema = read_json(&args.schema);
    let environment = Environment::new(&schema);

    (schema, environment)
}


