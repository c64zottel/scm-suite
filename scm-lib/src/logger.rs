//TODO: transform into trait

pub struct Logger;

impl Logger {
    pub fn log_string(&self, text: String) -> String {
        println!("{}", &text);
        text
    }
}

impl Default for Logger {
    fn default() -> Self {
        Self
    }
}