use crate::executable_content::ExecutionSystem;
use crate::scjson_serde::{Event, Transition};
use std::slice::Iter;

pub trait TransitionIterator<'a> {
    fn transition_iter(&self) -> Iter<'a, Transition>;
}


pub fn enabled_eventless_transitions<'a>(transitions: impl TransitionIterator<'a>, executor: &mut ExecutionSystem) -> Vec<&'a Transition> {
    transitions.transition_iter()
        .filter(|t|
            t.is_eventless()
                && !t.is_internal() // rethink! keep naming (get_enabled_transition) aligned
                && t.condition.as_ref().map_or(true, |text| executor.evaluate_condition(&text))
        ).collect::<Vec<&Transition>>()
}


pub fn calculate_target<'a>(transitions: &Vec<&'a Transition>) -> &'a Transition {
    transitions.iter().find(|t| !t.is_internal()).copied().unwrap()
}


pub fn get_enabled_transition<'a>(transitions: impl TransitionIterator<'a>, event: &Event, executor: &mut ExecutionSystem) -> Vec<&'a Transition> {
    transitions.transition_iter()
        .filter(|transition|
            !transition.is_internal()
                && transition.is_enabled(Some(event), executor))
        .collect::<Vec<&Transition>>()
}

impl Transition {
    pub fn is_event_empty(&self) -> bool {
        match &self.event {
            Some(event) if !event.event.is_empty() => { false }
            _ => { true }
        }
    }

    pub fn is_enabled(&self, event: Option<&Event>, executor: &mut ExecutionSystem) -> bool {
        event.map_or(true, |e| self.reacts_on(e))
            && self.condition.as_ref().map_or(true, |text| executor.evaluate_condition(&text))
    }

    pub fn reacts_on(&self, event: &Event) -> bool {
        if let Some(x) = &self.event {
            return x.event.eq(&event.event);
        }
        return true; //if None, then: targetless
    }

    pub fn is_internal(&self) -> bool {
        self.target.is_none()
    }

    pub fn is_eventless(&self) -> bool {
        self.event.is_none()
    }
}