#![allow(dead_code)]

use std::fmt::{Display, Formatter};
use rhai::{Dynamic, ImmutableString, Scope};

pub struct RhaiExecutor {
    script_engine: rhai::Engine,
    scope: Scope<'static>,
}

// TODO: maybe impl: https://www.w3.org/TR/scxml/#SystemVariables
// _event - current event
//   name
//   type
//   origin
//   data
#[derive(Default, Clone)]
struct RhaiEvent {
    name: String,
    argument: String,
}

impl RhaiEvent {
    fn name(&mut self) -> String { self.name.clone() }
    fn set_name(&mut self, name: String) { self.name = name; }
    fn argument(&mut self) -> String { self.argument.clone() }
    fn set_argument(&mut self, arg: String) { self.argument = arg; }
}

impl Display for RhaiEvent {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str("hellasdf")
    }
}

impl RhaiExecutor {
    fn create_scope(name: String) -> Scope<'static, 8> {
        use uuid::Uuid;
        let mut scope = Scope::new();
        scope.push_constant("__sessionid", Uuid::new_v4());
        scope.push_constant("__name", name);
        scope.push("__event", RhaiEvent { name: "dummy_event".to_string(), argument: "dummy_argument".to_string() });
        scope
    }

    pub fn new(name: String) -> RhaiExecutor {
        let mut rhai_executor = RhaiExecutor { script_engine: rhai::Engine::new(), scope: Self::create_scope(name) };
        rhai_executor.init();
        rhai_executor.script_engine
            .register_type::<RhaiEvent>()
            .register_get("name", RhaiEvent::name)
            .register_get("argument", RhaiEvent::argument);
        rhai_executor
    }

    pub fn execute_script(&mut self, text: &str) {
        self.script_engine.run_with_scope(&mut self.scope, text).unwrap();
    }

    pub fn evaluate_condition(&mut self, text: &str) -> bool {
        self.script_engine.eval_with_scope::<bool>(&mut self.scope, text).expect("Script must return bool.")
    }


    fn scm_print<T: Display>(x: &mut T) {
        println!("scm_print(): put up a good show: {x}!");
    }

    fn init(&mut self) {
        self.script_engine
            .register_fn("scm_print", Self::scm_print::<i64>)
            .register_fn("scm_print", Self::scm_print::<ImmutableString>);
        // engine.register_fn("print", show_it::<i64>)
        // .register_fn("print", show_it::<bool>)
        // .register_fn("print", show_it::<ImmutableString>);
    }

    pub(crate) fn set_event(&mut self, event_name: &String, argument: &String) {
        let dyn_event: &mut Dynamic = self.scope.get_mut("__event").unwrap();
        let mut dyn_event = dyn_event.write_lock::<RhaiEvent>().unwrap();
        dyn_event.name = event_name.clone();
        dyn_event.argument = argument.clone()
    }
}

