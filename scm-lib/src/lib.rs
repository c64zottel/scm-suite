use std::collections::HashMap;
use std::fmt::{Display, Formatter};
use futures::executor::block_on;
use futures::future::join_all;
use tokio::sync::mpsc::{channel, Receiver, Sender};
use serde_json::Value;
use tokio::task::JoinHandle;
use crate::communication::{SMProtocol, CHANNEL_SIZE, ManageSMProtocol, TargetEvent};
use crate::communication::InterSMProtocol::RaiseEvent;
use crate::scjson_serde::{RootElement, StateTypes, Transition};
use crate::state_machine::run_state_machine;

pub mod scjson_serde;
pub mod communication;
pub mod scjson_state_helpers;
mod state_machine_helpers;
mod executable_content;
mod state_machine;
mod rhai_environment;
mod logger;
mod scjson_transition_helpers;

pub type TargetMachine = String;

pub struct SMManager {
    pub senders: HashMap<TargetMachine, Sender<SMProtocol>>,
    pub handles: Vec<JoinHandle<()>>,
}

impl Display for SMManager {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let v = self.senders.keys().cloned().collect::<Vec<String>>().join(" ");
        f.write_str(&format!("{:?}", v))
    }
}

impl SMManager {
    pub fn new() -> Self {
        Self {
            senders: Default::default(),
            handles: vec![],
        }
    }
    pub fn send_event<M, T>(&self, machine: M, event: T)
        where T: Into<TargetEvent>,
              M: Into<String>
    {
        let sender = self.senders.get(&machine.into()).unwrap();
        let f = sender.send(SMProtocol::InterSMProtocol(RaiseEvent(event.into())));
        block_on(f).expect("sending event failed");
    }

    pub fn block_until_finished(self) {
        let iter = self.handles.into_iter();
        let f = join_all(iter);
        block_on(f);
    }
}

//TODO: How to run a SM to completion? Would be good for testing...
//TODO: Think about system events: SM can react on SMProtocol events -> tight on rhai?
//TODO: restructure: remove async, remove tokio dependency from clients
pub fn init_state_machines_from_json(json: Value) -> (SMManager, Receiver<SMProtocol>) {
    let root_element = serde_json::from_value::<RootElement>(json.clone()).unwrap();

    let (app_sender, app_receiver) = channel::<SMProtocol>(CHANNEL_SIZE);
    let mut sm_manager = SMManager::new();
    // to respond to app requests?
    // sm_manager.senders.insert("sm_application_64".to_string(), app_sender.clone()); // do we need this? why?

    for sm in root_element {
        tracing::debug!( StateMachine = sm.state_machine.to_string(), States = format!("{:?}", sm.states));
        let (sm_sender, sm_receiver) = channel::<SMProtocol>(CHANNEL_SIZE);
        sm_manager.senders.insert(sm.state_machine.clone(), sm_sender.clone());

        let app_sender = app_sender.clone();
        let handle = tokio::spawn(
            async move {
                run_state_machine(sm_receiver, sm_sender, app_sender, sm).await
            }
        );
        sm_manager.handles.push(handle);
    }

    for (_, sender) in &sm_manager.senders {
        let f = sender.send(SMProtocol::ManageSMProtocol(ManageSMProtocol::Init(sm_manager.senders.clone())));
        block_on(f).unwrap();
    }

    (sm_manager, app_receiver)
}

