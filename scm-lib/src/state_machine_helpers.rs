#![allow(dead_code)]
use std::hash::{Hash, Hasher};

use crate::{StateTypes, Transition};
use crate::executable_content::ExecutionSystem;
use crate::scjson_serde::{Event, ExecutableContent, ExecutableContentItems, Log};

impl Hash for Transition {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.event.iter().next().unwrap().event.hash(state);
        self.target.iter().next().hash(state);
    }
}

impl StateTypes {
    pub fn append_transition(&mut self, transition: Transition) {
        match self { StateTypes::State(state) => { state.transitions.push(transition) } }
    }

    pub fn transitions(&self) -> &Vec<Transition> {
        match self {
            StateTypes::State(s) => { &s.transitions }
        }
    }

    pub fn on_entry_content(&self) -> &ExecutableContent {
        match self {
            StateTypes::State(s) => { &s.on_entry }
        }
    }

    pub fn on_exit_content(&self) -> &ExecutableContent {
        match self {
            StateTypes::State(s) => { &s.on_exit }
        }
    }

    pub fn is_target(&self, target: &String) -> bool {
        match self {
            StateTypes::State(s) => { s.id.eq(target) }
        }
    }

    pub fn transition_for_event(&self, event: &Event) -> Option<&Transition> {
        for transition in self.transitions() {
            if transition.reacts_on(event) {
                return Some(transition);
            }
        }
        return None;
    }
}

pub async fn execute_internal_transitions(configuration: &StateTypes, executor: &mut ExecutionSystem) {
    let ecs = configuration.transitions().iter()
        .filter(|transition|
            transition.is_internal()
                && transition.is_enabled(None, executor)).collect();

    executor.run_transition_content(&ecs).await;
}

fn create_transition(condition: Option<&str>, log_text: &str, target: bool, event: bool) -> Transition
{
    Transition {
        condition: condition.and_then(|t| Some(t.to_string())),
        event: if event { Some(Event::from("random_event")) } else { None },
        executable_content: vec![ExecutableContentItems::Log(Log { message: log_text.to_string(), ..Default::default() })],
        target: if target { Some("random_target".to_string()) } else { None },
        ..Default::default()
    }
}

#[tokio::test]
async fn test_x() {
    use tokio::sync::mpsc::channel;

    let s1 = empty_states(String::from("S1"), vec![
        create_transition(None, "transitioning, easy case", false, false),
        create_transition(Some("false"), "not transitioning because condition false", false, false),
        create_transition(Some("true"), "transitioning, condition true", false, false),
        create_transition(Some("true"), "not transitioning with target", true, false),
        create_transition(Some("true"), "not transitioning with random event", false, true),
    ]);
    let (sender, _receiver) = channel::<crate::communication::SMProtocol>(10);
    let mut executor = ExecutionSystem::new(sender, "test machine".to_string(), None);

    execute_internal_transitions(&s1, &mut executor).await;
}


fn empty_states(id: String, transitions: Vec<Transition>) -> StateTypes {
    StateTypes::State(crate::scjson_serde::State {
        id,
        transitions,
        on_entry: vec![ExecutableContentItems::Log(Log { message: "entry".to_string(), ..Default::default() })],
        on_exit: vec![ExecutableContentItems::Log(Log { message: "exit".to_string(), ..Default::default() })],
    })
}

fn state_with_event_and_executable_content() -> StateTypes {
    StateTypes::State(crate::scjson_serde::State {
        id: "S1".to_string(),
        on_entry: vec![ExecutableContentItems::Log(Log { message: "entry".to_string(), ..Default::default() })],
        on_exit: vec![ExecutableContentItems::Log(Log { message: "exit".to_string(), ..Default::default() })],
        transitions: vec![
            Transition {
                event: Option::from(Event::from("e")),
                executable_content: vec![
                    ExecutableContentItems::Log(Log { label: None, message: "< transitioning >".to_string() }),
                ],
                ..Default::default()
            },
        ],
    })
}

// #[test]
// // transition:
// // - with target
// // - eventless
// fn test_1_with_event_transition()
// {
//     let mut s1 = state_with_event_and_executable_content();
//     s1.append_transition(Transition { executable_content: vec![ExecutableContentItems::Log(Log { label: None, message: "< transitioning >".to_string() })], target: Some("S2".to_string()), ..Default::default() });
//     let mut result = run_exiting_state_content(&mut s1);
//     assert_eq!(result.len(), 0);
// }
//
// #[test]
// fn test_no_event_transition()
// {
//     let mut s1 = state_with_event_and_executable_content();
//     s1.append_transition(Transition::default());
//
//     let result = run_exiting_state_content(&mut s1);
//     assert_eq!(result.len(), 1);
// }

// pub fn exit_states_eventless(configuration: &Vec<StateTypes>, event: &Event) -> Vec<&StateTypes> {
//     configuration.iter().flat_map(|state| {
//         state.transitions()
//             .iter()
//             .filter_map(|t| { t.reacts_on(event).then_some(state) }).collect::<Vec<&StateTypes>>()
//     }).collect::<Vec<&StateTypes>>()
// }

pub fn exit_states_eventless_2(configuration: &Vec<StateTypes>) -> Vec<&StateTypes> {
    configuration.iter().flat_map(|state| {
        state.transitions()
            .iter()
            .filter_map(|t| { t.event.is_none().then_some(state) }).collect::<Vec<&StateTypes>>()
    }).collect::<Vec<&StateTypes>>()
}

pub fn get_transitions<'a>(configuration: &'a Vec<StateTypes>, event: &'a Event) -> Vec<&'a Transition> {
    configuration.iter().flat_map(|state| {
        state.transitions().iter().filter(|t| t.reacts_on(event)).collect::<Vec<&Transition>>()
    }).collect::<Vec<&Transition>>()
}

pub fn get_eventless_transitions(configuration: &Vec<StateTypes>) -> Vec<&Transition> {
    configuration.iter().flat_map(|state| {
        state.transitions().iter().filter(|t| t.event.is_none()).collect::<Vec<&Transition>>()
    }).collect::<Vec<&Transition>>()
}


