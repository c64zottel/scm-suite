use std::collections::HashMap;
use tokio::sync::mpsc::Sender;
use tracing::{debug, info};
use crate::communication::{Delay, ManageSMProtocol, send_message, SMProtocol, TargetEvent};
use crate::communication::InterSMProtocol::{RaiseDelayedEvent, RaiseEvent};
use crate::communication::ManageSMProtocol::AllSystemsDown;
use crate::communication::SMProtocol::InterSMProtocol;
use crate::logger::Logger;
use crate::rhai_environment::RhaiExecutor;
use crate::scjson_serde::{Event, ExecutableContent, StateTypes, Transition};
use crate::scjson_serde::ExecutableContentItems::*;
use crate::TargetMachine;


pub struct ExecutionSystem {
    sender: Sender<SMProtocol>,
    pub senders: HashMap<TargetMachine, Sender<SMProtocol>>,
    machine: String,
    script_executor: Option<RhaiExecutor>,
    log_system: Logger,
}


impl ExecutionSystem {
    pub async fn execute_executable_content(&mut self, content: &ExecutableContent) {
        async {
            for exc in content {
                match exc {
                    Log(log) => {
                        debug!("{:?}", log);
                        self.log_system.log_string(log.message.to_string());
                    }
                    Script(script) => {
                        self.script_executor.as_mut().unwrap().execute_script(&script);
                    }
                    Raise(raise) => {
                        debug!("{:?}", raise);
                        let machine = raise.machine.clone().unwrap_or_else(|| self.machine.to_owned());
                        let target_event = TargetEvent {
                            name: raise.event.event.clone(),
                            argument: raise.event.argument.clone(),
                        };
                        if let Some(delay) = raise.delay {
                            let message = InterSMProtocol(RaiseDelayedEvent(delay as Delay, target_event));
                            self.send_to_dispatch(machine, message).await;
                        } else {
                            let message = InterSMProtocol(RaiseEvent(target_event));
                            self.send_to_dispatch(machine, message).await;
                        }
                    }
                    Shutdown(shutdown) => {
                        if !shutdown.condition.as_ref().map_or(true, |text| self.evaluate_condition(&text)) { continue; }

                        let last_machine_closing = self.senders.len() == 1;
                        let message = SMProtocol::ManageSMProtocol(ManageSMProtocol::RemoveStateMachine(self.machine.to_owned()));
                        self.broadcast(message).await;
                        let message = SMProtocol::ManageSMProtocol(ManageSMProtocol::ShutDown(shutdown.comment.to_owned().unwrap_or_default()));
                        let machine = self.machine.to_owned();
                        self.send_to_dispatch(machine, message).await;
                        if last_machine_closing {
                            println!("\nclosing time");
                            self.sender.send(SMProtocol::ManageSMProtocol(AllSystemsDown)).await.unwrap();
                        }
                    }
                    Send(send) => {
                        send_message(&self.sender, SMProtocol::CustomMessage(send.string.to_owned().unwrap_or_default())).await;
                    }
                    StopExecutionIf(condition_text) => {
                        if self.evaluate_condition(condition_text) {
                            break;
                        }
                    }
                }
            }
        }.await;
    }

    async fn broadcast(&self, message: SMProtocol) {
        for sender in self.senders.values() {
            send_message(&sender, message.clone()).await;
        }
    }

    pub fn evaluate_condition(&mut self, text: &String) -> bool {
        let result = self.script_executor.as_mut().unwrap().evaluate_condition(text);
        result
    }

    pub(crate) fn new(sender: Sender<SMProtocol>, machine: String, logger: Option<Logger>) -> ExecutionSystem {
        Self {
            sender,
            senders: Default::default(),
            machine: machine.clone(),
            script_executor: Some(RhaiExecutor::new(machine)),
            log_system: logger.unwrap_or_default(),
        }
    }

    async fn send_to_dispatch(&self, machine: TargetMachine, message: SMProtocol) {
        //no local SM? then default to external (client application) channel
        // let sender = self.senders.get(&machine).unwrap_or(&self.sender);
        if let Some(sender) = self.senders.get(&machine) {
            send_message(sender, message).await;
        } else { info!("sender not reachable {}", machine) };
    }

    pub async fn run_exit_content(&mut self, state: &StateTypes) {
        self.execute_executable_content(state.on_exit_content()).await;
    }

    pub async fn run_entry_content(&mut self, state: &StateTypes) {
        self.execute_executable_content(state.on_entry_content()).await;
    }

    pub async fn run_transition_content(&mut self, transitions: &Vec<&Transition>) {
        for transition in transitions {
            self.execute_executable_content(&transition.executable_content).await;
        }
    }

    pub fn set_event(&mut self, event: &Event) {
        self.script_executor.as_mut().unwrap().set_event(&event.event, &event.argument);
    }
}
