use std::fmt::{Debug, Display, Formatter};
use std::slice::Iter;
use crate::scjson_serde::{Event, State, StateTypes, Transition};
use crate::scjson_transition_helpers::TransitionIterator;

impl<'a> TransitionIterator<'a> for &'a StateTypes {
    fn transition_iter(&self) -> Iter<'a, Transition> {
        match self {
            StateTypes::State(state) => state.transitions.iter()
        }
    }
}


impl Display for StateTypes {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Ok(
            f.write_str(format!("{}", match self {
                StateTypes::State(s) => { &s.id }
            }).as_ref()).unwrap()
        )
    }
}

impl Debug for State {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(&*format!(
            "id: {}  {:?}", self.id, self.transitions
        ))
    }
}


impl PartialEq<String> for StateTypes {
    fn eq(&self, other: &String) -> bool {
        match self {
            StateTypes::State(s) => s.id.eq(other)
        }
    }
}

impl PartialEq<String> for &StateTypes {
    fn eq(&self, other: &String) -> bool { (**self).eq(other) }
}


impl From<&str> for Event {
    fn from(name: &str) -> Self {
        Self {
            event: name.to_string(),
            argument: "".to_string(),
        }
    }
}