use std::collections::HashMap;
use tokio::sync::mpsc::Sender;
use crate::TargetMachine;

//TODO: rename to protocols.rs
//TODO: create a simple SM for SMs: (How to kill?)
//CREATED --(MSMP::Init)--> RUNNING --(MSMP::Shutdown)--> FINISHED

pub const CHANNEL_SIZE: usize = 100;

pub type Delay = u16;

#[derive(Debug, Clone)]
pub struct TargetEvent {
    pub name: String,
    pub argument: String,
}

impl From<&str> for TargetEvent {
    fn from(name: &str) -> Self {
        Self { name: name.to_string(), argument: "".to_string() }
    }
}

#[derive(Debug, Clone)]
pub enum SMProtocol {
    InterSMProtocol(InterSMProtocol),
    ManageSMProtocol(ManageSMProtocol),
    CustomMessage(String),
}

#[derive(Debug, Clone)]
pub enum InterSMProtocol {
    RaiseEvent(TargetEvent),
    RaiseDelayedEvent(Delay, TargetEvent),
}

#[derive(Debug, Clone)]
pub enum ManageSMProtocol {
    ShutDown(String),
    AllSystemsDown,
    Init(HashMap<TargetMachine, Sender<SMProtocol>>),
    AddSM(Sender<InterSMProtocol>),
    RemoveStateMachine(TargetMachine),
    RequestConfiguration(Sender<String>),
}


pub(crate) async fn send_message(sender: &Sender<SMProtocol>, message: SMProtocol) {
    sender.send(message).await.unwrap_or_else(|s| {
        println!("Error sending {:?}", s);
    });
}