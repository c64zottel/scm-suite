use serde::{Deserialize, Deserializer};
use serde_derive::*;

pub type ConditionText = String;

#[derive(Clone, PartialEq, Debug, Deserialize, Serialize, Default)]
#[serde(deny_unknown_fields)]
pub struct Event {
    pub event: String,
    pub argument: String,
}

pub type ExecutableContent = Vec<ExecutableContentItems>;

#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub enum ExecutableContentItems {
    #[serde(rename = "raise")]
    Raise(Raise),
    #[serde(rename = "send")]
    Send(Send),
    #[serde(rename = "log")]
    Log(Log),
    #[serde(rename = "shutdown")]
    Shutdown(Shutdown),
    #[serde(rename = "stop_execution_if")]
    StopExecutionIf(StopExecutionIf),
    #[serde(rename = "script")]
    Script(Script),
}

pub type IdType = String;
pub type RootElement = Vec<StateMachineType>;
pub type ScriptText = String;

#[derive(Clone, PartialEq, Deserialize, Serialize, Default)]
#[serde(deny_unknown_fields)]
pub struct State {
    #[serde(default)]
    pub id: IdType,
    #[serde(default)]
    pub on_entry: ExecutableContent,
    #[serde(default)]
    pub on_exit: ExecutableContent,
    #[serde(default)]
    pub transitions: Vec<Transition>,
}

#[derive(Clone, PartialEq, Debug, Deserialize, Serialize, Default)]
#[serde(deny_unknown_fields)]
pub struct StateMachineType {
    #[serde(rename = "StateMachine")]
    #[serde(default)]
    pub state_machine: String,
    pub description: Option<String>,
    #[serde(default)]
    pub initial_transition: InitialTransition,
    #[serde(default)]
    pub states: Vec<StateTypes>,
}

#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub enum StateTypes {
    State(State),
}

pub type Target = String;

#[derive(Clone, PartialEq, Debug, Deserialize, Serialize, Default)]
#[serde(deny_unknown_fields)]
pub struct Transition {
    pub condition: Option<ConditionText>,
    pub event: Option<Event>,
    #[serde(default)]
    pub executable_content: ExecutableContent,
    pub kind: Option<String>,
    pub target: Option<Target>,
}

#[derive(Clone, PartialEq, Debug, Deserialize, Serialize, Default)]
#[serde(deny_unknown_fields)]
pub struct InitialTransition {
    #[serde(default)]
    pub executable_content: ExecutableContent,
    #[serde(default)]
    pub target: Target,
}

#[derive(Clone, PartialEq, Debug, Deserialize, Serialize, Default)]
#[serde(deny_unknown_fields)]
pub struct Log {
    pub label: Option<String>,
    #[serde(default)]
    pub message: String,
}

#[derive(Clone, PartialEq, Debug, Deserialize, Serialize, Default)]
#[serde(deny_unknown_fields)]
pub struct Raise {
    #[serde(default)]
    #[serde(deserialize_with = "from_delay_string")]
    pub delay: Option<i64>,
    #[serde(default)]
    pub event: Event,
    pub machine: Option<String>,
}

pub type Script = String;

#[derive(Clone, PartialEq, Debug, Deserialize, Serialize, Default)]
#[serde(deny_unknown_fields)]
pub struct Send {
    pub script: Option<ScriptText>,
    pub string: Option<String>,
}

#[derive(Clone, PartialEq, Debug, Deserialize, Serialize, Default)]
#[serde(deny_unknown_fields)]
pub struct Shutdown {
    pub comment: Option<String>,
    pub condition: Option<ConditionText>,
}

pub type StopExecutionIf = String;


fn from_delay_string<'de, D>(deserializer: D) -> Result<Option<i64>, D::Error>
    where
        D: Deserializer<'de>,
{
    let delay_string: Option<i64> = Option::deserialize(deserializer)?;
    match delay_string {
        Some(s) => {
            // Here you can customize the logic for converting the delay string to an i64
            if s > 10000 { panic!("not going so well..") }
            Ok(Some(s))
        }
        None => Ok(None),
    }
}