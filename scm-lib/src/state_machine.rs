use std::time::Duration;
use tokio::sync::mpsc::{Receiver, Sender};
use tracing::{debug, info};
use tracing_subscriber::fmt::format::{DefaultFields, Format, Full};
use tracing_subscriber::fmt::Subscriber;
use crate::communication::{ManageSMProtocol, SMProtocol};
use crate::communication::InterSMProtocol::{RaiseDelayedEvent, RaiseEvent};
use crate::executable_content::ExecutionSystem;
use crate::scjson_serde::{Event, State, StateMachineType, StateTypes, Transition};
use crate::state_machine_helpers::*;
use crate::scjson_transition_helpers::*;
use crate::TargetMachine;

fn get_subscriber() -> Subscriber<DefaultFields, Format<Full, ()>> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::TRACE)
        .with_line_number(true)
        .without_time()
        .finish()
}

pub async fn run_state_machine(mut receiver: Receiver<SMProtocol>, local_sender: Sender<SMProtocol>, app_sender: Sender<SMProtocol>, sm: StateMachineType) {
    let mut sm_runtime = SMRuntime::new(local_sender, sm.to_owned(), app_sender);

    let _default_guard = tracing::subscriber::set_default(get_subscriber());

    loop {
        if let Some(msg) = receiver.recv().await {
            debug!("received: ({}) {:?}", &sm.state_machine, &msg);
            match msg {

                // SMProtocol::RequestConfiguration(sender) => {
                //     println!("puh")
                //     sender.send("hm".to_string()).unwrap();
                // }
                SMProtocol::InterSMProtocol(inter) => match inter {
                    RaiseEvent(event) => {
                        sm_runtime.apply_event(&Event { event: event.name, argument: event.argument }).await;
                    }
                    // Storing the event for myself for later...
                    RaiseDelayedEvent(delay, event) => {
                        let dest = sm_runtime.local_sender.clone();
                        info!("raising delayed event: {} with argument {} with delay: {} ", event.name, event.argument, delay);
                        tokio::task::spawn(async move {
                            tokio::time::sleep(Duration::from_millis(delay as u64)).await;
                            dest.send(SMProtocol::InterSMProtocol(RaiseEvent(event))).await.unwrap();
                        }).await.unwrap();
                    }
                }
                SMProtocol::ManageSMProtocol(manage) => match manage {
                    ManageSMProtocol::Init(senders) => {
                        sm_runtime.execution_system.senders.extend(senders);
                        sm_runtime.start().await;
                    }
                    ManageSMProtocol::AddSM(_) => {}
                    ManageSMProtocol::RemoveStateMachine(target) => {
                        sm_runtime.remove_state_machine(target);
                    }
                    ManageSMProtocol::RequestConfiguration(_) => {}
                    ManageSMProtocol::ShutDown(comment) => {
                        info!("{} {comment}", sm_runtime.name());
                        break;
                    }
                    ManageSMProtocol::AllSystemsDown => {}
                }
                SMProtocol::CustomMessage(_) => { panic!("none of my business") }
            }
        }
    }
}


struct SMRuntime {
    state_machine: StateMachineType,
    //For now, the SM can be in only one State at a time
    pub(crate) current_configuration: StateTypes,
    pub(crate) execution_system: ExecutionSystem,
    pub local_sender: Sender<SMProtocol>,
}

impl SMRuntime {
    fn name(&self) -> &String {
        &self.state_machine.state_machine
    }

    fn remove_state_machine(&mut self, target: TargetMachine) {
        self.execution_system.senders.remove(target.as_str());
    }

    fn new(local_sender: Sender<SMProtocol>, state_machine: StateMachineType, app_sender: Sender<SMProtocol>) -> SMRuntime {
        let machine = state_machine.state_machine.to_owned();
        //We copy the initial_transition into an eventless_transition, that simplifies the startup
        let transition = Transition {
            executable_content: state_machine.initial_transition.executable_content.clone(),
            target: Some(state_machine.initial_transition.target.to_owned()),
            event: Some(Event::from("__internal_start_event__")),
            ..Default::default()
        };
        SMRuntime {
            state_machine,
            current_configuration: StateTypes::State(State {
                id: "start_dummy".to_string(),
                on_entry: vec![],
                on_exit: vec![],
                transitions: vec![transition],
            }),
            execution_system: ExecutionSystem::new(app_sender, machine, None),
            local_sender,
        }
    }

    async fn start(&mut self) {
        info!("start: ({})", self.name());
        self.apply_event(&Event::from("__internal_start_event__")).await;
    }

    // Execution order:
    // document order for hierarchical states
    // - eventless transitions without target
    // - exits from states with transitions with targets (independent of event)
    // - entries from target states
    /// Internal transitions are transitions without any target
    /// They do not trigger executable content in on_entry/on_exit tags
    /// External transitions have a target, and therefor trigger executable content in states.
    ///
    /// Algorithm to apply an event:
    /// 1. execute all internal transitions
    /// 2. enabled_transitions = get enabled transitions
    /// 3. break if enabled_transitions is empty
    /// 4. execute EXIT content on source states for enabled transitions
    /// 5. execute TRANSITION content on enabled transitions
    /// 6. target_transitions = calculate targets from enabled_transitions
    /// 7. new_config = calculate new config from target_transitions
    /// 8. execute ENTRY content of new_config
    /// 9. current_config = new_config
    /// 10. goto 1
    async fn apply_event(&mut self, event: &Event) {
        self.execution_system.set_event(event);
        let mut enabled_transitions = get_enabled_transition(&self.current_configuration, event, &mut self.execution_system);
        debug!("enabled transitions: {:?}", enabled_transitions);
        let mut new_configuration = self.current_configuration.to_owned();
        loop {
            execute_internal_transitions(&self.current_configuration, &mut self.execution_system).await;
            if enabled_transitions.is_empty() { break; }

            self.execution_system.run_exit_content(&new_configuration).await;
            self.execution_system.run_transition_content(&enabled_transitions).await;

            let target_transition = calculate_target(&enabled_transitions);

            let new_config = Self::get_state_by_name(&self.state_machine, &target_transition.target.as_ref().unwrap());
            enabled_transitions = enabled_eventless_transitions(&*new_config, &mut self.execution_system);
            self.execution_system.run_entry_content(&new_config).await;
            new_configuration = new_config.to_owned();
        }
        self.current_configuration = new_configuration;
    }

    fn get_state_by_name<'a>(state_machine: &'a StateMachineType, state_name: &String) -> &'a StateTypes {
        state_machine.states.iter()
            .find(|s| (*s).eq(state_name))
            .expect(&format!("There is no state {}", &state_name))
    }
}
