use crate::test_helper::*;

mod test_helper;


#[tokio::test(flavor = "multi_thread")]
async fn test_shutdown() {
    let (runtime, receiver) = init_from_file("tests/test-machines/1_Shutdown_multi.yaml").await;
    runtime.send_event("SM1", "start");
    let handle = spawn_trace_catcher(receiver).await;

    runtime.block_until_finished();

    let expected_log = "TRACE:state_1_on_entrytransition_state_1_to_state_2state_2_on_entrystate_2_on_entrystate_2_on_entrystate_2_on_entry".to_string();
    assert_eq!(handle, expected_log);
}


#[tokio::test(flavor = "multi_thread")]
async fn test_shutdown_multi() {
    let (runtime, receiver) = init_from_file("tests/test-machines/1_Shutdown.yaml").await;
    runtime.send_event("SM1", "start");
    let handle = spawn_trace_catcher(receiver).await;

    runtime.block_until_finished();

    let expected_log = "TRACE:state_1_on_entrystate_2_transitionstate_2_on_entrystate_2_transitionfinal_state_on_entry".to_string();
    assert_eq!(handle, expected_log);
}

#[tokio::test(flavor = "multi_thread")]
async fn system_variables() {
    let (runtime, receiver) = init_from_file("tests/test-machines/test_2_rhai_system_variables.yaml").await;
    runtime.send_event("SM1", "start");
    let handle = spawn_trace_catcher(receiver).await;

    runtime.block_until_finished();

    let expected_log = "TRACE:its over now";
    assert_eq!(handle, expected_log);
}

const INTERNAL_TRANSITION: &str = r#"
- StateMachine: SM1
  initial_transition: { target: state_1 }
  states:
    - State:
        id: state_1
        on_entry: [ { send: { string: "state_1_on_entry" } } ]
        on_exit:  [ { send: { string: "state_1_on_exit" } } ]
        transitions:
          - { executable_content: [ { send: { string: "state_1_internal_transition" } } ] }
          - { condition: "true", executable_content: [ { send: { string: "state_1_internal_transition_condition" } } ] }
          - { condition: "false", executable_content: [ { send: { string: "not to be printed" } } ] }
          - { target: "state_2", executable_content: [ { log: { message: "error, with target" } } ] }
    - State:
        id: state_2
        on_entry: [ { send: { string: "state_2_on_entry" } }  ]
        on_exit:  [ { send: { string: "not to be printed" } } ]
        transitions:
          - { executable_content: [ { shutdown: { comment: "shutdown SM1" } } ] }
"#;

#[tokio::test(flavor = "multi_thread")]
async fn test_x() {
    let (runtime, receiver) = init_from_string(INTERNAL_TRANSITION).await;
    runtime.send_event("SM1", "start");
    let handle = spawn_trace_catcher(receiver).await;

    runtime.block_until_finished();

    let expected_log = "TRACE:state_1_on_entrystate_1_on_exitstate_2_on_entry".to_string();
    assert_eq!(handle, expected_log);
}


// // - transitions ECs are executed in document order
// //executing EC
// // - we do not have internal/external like SCXML, but we do:
// //   - behave like internal (only execute EC from the transition): target is missing
// //   - behave like external (execute EC from state (on_exit/on_entry) and transition): self targeting target
// //   - active_transition    has_target  action:
// //   -      x                   x       on_exit + EC from transition + on_entry
// //   -                          x       on_exit + EC from transition + on_entry
// //   -      x                           EC from transition - no state change
// //   -                                  EC from transition - no state change
// const STATE_TYPES_TEST: &str = r#"
// - StateMachine: SM1
//   initial_transition: { target: state_1 }
//   states:
//     - State:
//         id: S1
//         on_entry: [ { log: { message: "entering S1" } } ]
//         on_exit:  [ { log: { message: "leaving S1" } } ]
//         transitions:
//           - { event: e, target: S2, executable_content: [ { log: { message: "See you in state S1." } } ] }
//           - { executable_content: [ { log: { message: "every day logging." } } ] }
//     - State:
//         id: S2
//         on_entry: [ { log: { message: "entering S2" } } ]
//         on_exit:  [ { log: { message: "leaving S2" } } ]
//         transitions:
//           - { event: e, target: S1, executable_content: [ { log: { message: "See you in state S1." } } ] }
// "#;
//
// #[ignore]
// #[tokio::test(flavor = "multi_thread", worker_threads = 2)]
// async fn execution_order() {
//     let (runtime, _) = init_from_string(STATE_TYPES_TEST).await;
//     runtime.send_event("SM1", "e").await;
//     runtime.run_to_finish().await;
// }
//
// #[ignore]
// #[tokio::test(flavor = "multi_thread", worker_threads = 1)]
// async fn flip_flop() {
//     let (runtime, _) = init_from_string(FLIP_FLOP).await;
//     runtime.send_event("SM1", "flip").await;
//     runtime.run_to_finish().await;
//
//     // let expected_log = "SM1 flipping\nSM2 flipping\nSM1 flopping\nSM2 flopping\nSM1 flipping\n".to_string();
//     // println!("{:?}", x.get_logs());
//     // assert_eq!(x.get_logs(), expected_log);
// }
//
// #[ignore]
// #[tokio::test(flavor = "multi_thread", worker_threads = 1)]
// async fn calc_rhai() {
//     let (runtime, _) = init_from_string(CALC_RHAI).await;
//
//     runtime.send_event("SM1", "E").await;
//     runtime.run_to_finish().await;
//
//     // let expected_log = "running\nround trip\nround trip\nround trip\n".to_string();
//     // println!("{:?}", x.get_logs());
//     // assert_eq!(x.get_logs(), expected_log);
// }
//
// #[ignore]
// #[tokio::test(flavor = "multi_thread", worker_threads = 1)]
// async fn run_complex_test() {
//     let (runtime, _) = init_from_string(CALC_RHAI).await;
//
//     runtime.send_event("SM1", "E").await;
//     runtime.run_to_finish().await;
//
//     // let expected_log = "running\nround trip\nround trip\nround trip\n".to_string();
//     // println!("{:?}", assert_writer.get_logs());
//     // assert_eq!(x.get_logs(), expected_log);
// }
//
//
// const CALC_RHAI: &str = r#"
// - StateMachine: SM1
//   initial_transition:
//     target: S1
//     executable_content:
//       - log: { message: "running" }
//       - script: "let x = 1"
//   states:
//     - State:
//         id: S1
//         transitions:
//           - event: E
//             target: S1
//             executable_content:
//               - raise: { machine: SM1, event: "E", delay: 2000 }
//               - log: { message: "round trip" }
//               - script: |
//                     x = x + 1;
//                     print(x);
// "#;
//
// const FLIP_FLOP: &str = r#"
// - StateMachine: SM1
//   initial_transition: { target: flopped }
//   states:
//     - State:
//         id: flopped
//         transitions:
//           - { event: flip, target: flipped, executable_content: [
//           { log: { message: "SM1 flipping" } },
//           { raise: { machine: SM2, event: flip, delay: 1000 } },
//           ] }
//     - State:
//         id: flipped
//         transitions:
//           - { event: flop, target: flopped, executable_content: [
//           { log: { message: "SM1 flopping" } },
//           { raise: { machine: SM2, event: flop, delay: 1000 } },
//           ] }
// - StateMachine: SM2
//   initial_transition: { target: flopped }
//   states:
//     - State:
//         id: flopped
//         transitions:
//           - { event: flip, target: flipped, executable_content: [
//           { log: { message: "SM2 flipping" } },
//           { raise: { machine: SM1, event: flop, delay: 1000 } },
//           ] }
//     - State:
//         id: flipped
//         transitions:
//           - { event: flop, target: flopped, executable_content: [
//           { log: { message: "SM2 flopping" } },
//           { raise: { machine: SM1, event: flip, delay: 1000 } },
//           ] }
// "#;