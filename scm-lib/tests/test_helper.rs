use std::fs::File;
use std::io::Read;
// use std::sync::{Arc, Mutex};
use tokio::sync::mpsc::Receiver;
use tokio::task;

// use tracing::Level;
// use tracing_subscriber::{filter, Layer, Registry};
// use tracing_subscriber::fmt::MakeWriter;
// use tracing_subscriber::prelude::*;

use scm_lib::{init_state_machines_from_json, SMManager};
use scm_lib::communication::{ManageSMProtocol, SMProtocol};

// #[derive(Default, Clone)]
// pub struct AssertWriter {
//     lines: Arc<Mutex<String>>,
// }

pub async fn spawn_trace_catcher(mut receiver: Receiver<SMProtocol>) -> String {
    return task::spawn(async move {
        let mut string_bucket = String::from("TRACE:");
        loop {
            match receiver.recv().await {
                Some(sm_protocol) => {
                    match sm_protocol {
                        SMProtocol::CustomMessage(message) => string_bucket.push_str(&message),
                        SMProtocol::ManageSMProtocol(ManageSMProtocol::AllSystemsDown) => break,
                        _ => { panic!("I should not be here") }
                    }
                }
                _ => { panic!("I should not be here either") }
            }
        }
        string_bucket
    }).await.unwrap();
}

pub async fn init_from_file(path: &str) -> (SMManager, Receiver<SMProtocol>) {
    let mut file = File::open(&path).expect("Failed to open file");
    let mut json_string = String::default();
    file.read_to_string(&mut json_string).unwrap();

    let json = serde_yaml::from_str(&*json_string).unwrap();
    init_state_machines_from_json(json)
}

pub async fn init_from_string(json_string: &str) -> (SMManager, Receiver<SMProtocol>) {
    let json = serde_yaml::from_str(json_string).unwrap();
    init_state_machines_from_json(json)
}
//
// impl AssertWriter {
//     pub fn get_logs(&self) -> String { self.lines.lock().unwrap().to_string() }
//     pub fn reset(&mut self) { self.lines.lock().unwrap().clear(); }
// }
//
// impl io::Write for AssertWriter {
//     fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
//         let s = std::str::from_utf8(buf).unwrap();
//         self.lines.lock().unwrap().extend(s.chars());
//         Ok(buf.len())
//     }
//
//     fn flush(&mut self) -> io::Result<()> { Ok(()) }
// }
//
// impl<'a> MakeWriter<'a> for AssertWriter {
//     type Writer = AssertWriter;
//
//     fn make_writer(&'a self) -> Self::Writer {
//         self.clone()
//     }
// }
//
// // pub fn get_assert_writer() -> &'static mut MutexGuard<'static, AssertWriter> {
// //     AssertWriterSingleton.lock().as_mut().unwrap()
// // }
//
// pub fn get_assert_writer() -> AssertWriter {
//     let assert_writer = AssertWriter::default();
//
//     let filtered_layer = tracing_subscriber::fmt::layer()
//         .with_filter(filter::filter_fn(|metadata| {
//             metadata.level() == &Level::DEBUG
//         }));
//
//     let assertion_layer = tracing_subscriber::fmt::layer()
//         .with_writer(assert_writer.clone())
//         .without_time()
//         .with_level(false)
//         .with_target(false)
//         .with_filter(filter::filter_fn(|metadata| {
//             metadata.level() == &Level::DEBUG
//         }));
//
//     let subscriber = Registry::default()
//         .with(filtered_layer)
//         .with(assertion_layer);
//
//     tracing::subscriber::set_global_default(subscriber).unwrap();
//
//     assert_writer
// }